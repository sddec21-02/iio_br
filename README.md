# Buildroot configuration for running the IIO Cosimulation demo

### To use:

Inside of the /buildroot/ directory, call `make BR2_EXTERNAL=../iio_br/ cosim_defconfig`,
followed by `utils/brmake -j12`

### Author
Braedon Giblin <bgiblin@iastate.edu>, Senior Design Team SDDEC21-02

### About

This repository contains an external Buildroot directory that defines a Linux
system containing the IIO subsystem, a Dummy IIO driver, the IIO Daemon to serve
over a network, and some simple networking tools. These tools compromise a
minimum viable system that can execute IIO drivers and easily share the data
with the outside world.

#### Kernel Changes

The kernel changes made to the dummy IIO driver, as described under the
[following patch](patches/linux/linux-iio.patch), include modifying the buffered
reader to use remapped IO memory. These IO memory registers are defined under
the SystemC model. The dummy data we feed in to the model does not have any
relevance to the actual channel types, but are simple examples of data flowing
through the model. 

The [defconfig](configs/cosim_defconfig) provided enables the system to use a
Zynq 7000 chip, with a Cortex A9 Arm Processor. The config also provides several
instrucitons to buildroot on selecting the appropriate packages and kernel
configurations that are needed.

#### Extensions

This interface can easily be extended, by adding additional kernel patches to
supplement IIO functionality, adding in more packages, or modifying the kernel
configuration further.


